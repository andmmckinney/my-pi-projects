import time, signal, sys
import RPi.GPIO as GPIO

#constants
leverPin = 17
requiredHoldTimeS = 5
reactionTimeS = 0.5
earlyTimeoutS = 2
lateTimeoutS = 5

#GPIO setup
GPIO.setmode(GPIO.BCM)
GPIO.setup(leverPin,GPIO.IN)
leverDown = (GPIO.input(leverPin)==GPIO.LOW)

#reset temporary variables
t=0
trN = 0
nCorrect = 0
nEarly = 0
nLate = 0

data = []

while True:
    while t==0:
        print('')
        print('************')
        print('%d Correct') %nCorrect
        print('%d Early') %nEarly
        print('%d Late') %nLate
        print('************')
        print('')
        print('Waiting for press....')
        t=1
    while (GPIO.input(leverPin)==GPIO.LOW) and t==1: #lever down
        startTime = time.clock()
        print('Lever DOWN')
        while True:
            if (GPIO.input(leverPin) == GPIO.HIGH) and ((time.clock() - startTime) < requiredHoldTimeS): #lever up
                print('EARLY RELEASE')
                elapsed = (time.clock() - startTime)
                print('***Held %5.3f seconds.') %elapsed
                print('In early timeout for %d seconds') %earlyTimeoutS
                time.sleep(earlyTimeoutS)
                data.append(elapsed)
                trN = trN+1
                nEarly = nEarly+1
                t=0
                break
            if (GPIO.input(leverPin) == GPIO.HIGH) and ((time.clock() - startTime) > (requiredHoldTimeS)) and ((time.clock() - startTime) < (requiredHoldTimeS+reactionTimeS)) :
                print('Held to completion.')
                elapsed = (time.clock() - startTime)
                print('***Held %5.3f seconds.') %elapsed
                #dispense reward from solenoid
                data.append(elapsed)
                trN = trN+1
                nCorrect = nCorrect+1
                t=0
                break
            if ((time.clock() - startTime) >= (requiredHoldTimeS+reactionTimeS)) :
                print('LATE RELEASE')
                elapsed = (time.clock() - startTime)
                print('***Held %5.3f seconds.') %elapsed
                print('In late timeout for %d seconds') %lateTimeoutS
                time.sleep(lateTimeoutS)
                data.append(elapsed)
                trN = trN+1
                nLate = nLate+1
                t=0
                break