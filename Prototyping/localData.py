import time, signal, sys, socket
# Point towards Adafruit ADC library folder then load it
sys.path.append("/usr/share/adafruit/webide/repositories/Adafruit-Raspberry-Pi-Python-Code/Adafruit_ADS1x15")
from Adafruit_ADS1x15 import ADS1x15

#Initialize ADC reader for MQ-2
measurement=0;
ADS1115 = 0x01
adc = ADS1x15(ic=ADS1115)

while True:
    HOST = 'andrews-air.local'    # The remote host, in this case the laptop
    PORT = 50007              # The same port as used by the server
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    #s.sendall('Hello, world')
    data = s.recv(1024)
    s.close()
    print 'Received', repr(data)