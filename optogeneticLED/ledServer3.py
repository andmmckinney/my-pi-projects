import time, signal, sys, socket
import numpy as np
import pickle as p
import RPi.GPIO as GPIO
# Point towards Adafruit DAC library folder then load it
sys.path.append("/usr/share/adafruit/webide/repositories/Adafruit-Raspberry-Pi-Python-Code/Adafruit_MCP4725")
from Adafruit_MCP4725 import MCP4725
from lookUpTable import computePower

HOST = '' 
PORT = 9992         # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(999)
s.bind((HOST, PORT))
s.listen(10)
conn, addr = s.accept()
#s.connect((HOST, PORT))
#s.sendall('Hello, world')

data = conn.recv(1024)
data = p.loads(data)
print ('Received %d mW packet') %data
#s.close
# initialize control elements
dac = MCP4725(0x62)
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN)

#calculate voltage
#volts = computePower(data, "/home/pi/Desktop/rig3.txt")
volts = computePower(data, "/home/pi/Desktop/laser532nm.txt")
maxVal = 4096
voltageSource = 5.0
voltsInBits = [int((volts/voltageSource)*maxVal)]
print(voltsInBits)
d=0
nStims = 0

# main loop
while True:
    while (GPIO.input(17) == GPIO.HIGH):
        for val in voltsInBits:
            dac.setVoltage(val)
        if d==0:
            nStims = nStims+1
            print('FIRING: %d Optical Stimuli Sent') %nStims
            d=1
            signal=0
    while (GPIO.input(17) == GPIO.LOW):
        dac.setVoltage(-1) #zeros voltage
        while d==1 and GPIO.input(17)==GPIO.LOW:
            print('Off')
            if signal==0:
                s.listen(1)
                s.settimeout(3)
                try:
                    conn, addr = s.accept()
                    data = conn.recv(1024)
                    data = p.loads(data)
                    print 'Received', repr(data)
                    signal=1
                    d=0
                except socket.timeout:
                    d=0
                    print 'socket timeout: using previous power level'
                    continue
                except socket.error:
                    d=0
                    print 'socket error: see Andrew :) '
                    continue
            d=0
        volts = computePower(data, "/home/pi/Desktop/laser532nm.txt")
        #volts = computePower(data, "/home/pi/Desktop/rig3.txt")
        maxVal = 4096
        voltageSource = 5.0
        voltsInBits = [int((volts/voltageSource)*maxVal)]
        #print(voltsInBits)